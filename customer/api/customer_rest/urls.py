from django.urls import path
from .views import CustomerList, UserRegister, CustomerLogin, LogoutView


urlpatterns = [
    path("customers/", CustomerList.as_view(), name="customer-list" ),
    path("customers/create/", UserRegister.as_view(), name='create-customer'),
    path("customers/login/", CustomerLogin.as_view(), name="login"),
    path("customers/logout/", LogoutView.as_view(), name="logout"),
]
