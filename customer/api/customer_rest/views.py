from django.contrib.auth import login, logout
from rest_framework.authentication import SessionAuthentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status, generics, serializers
from .serializers import CustomerRegistrationSerializer,CustomerLoginSerializer,UserSerializer
from .models import Customer

class CustomerList(generics.ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = UserSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)



class UserRegister(APIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Customer.objects.all()
    serializer_class = CustomerRegistrationSerializer

    def create_customer(self, serializer):
        if serializer.is_valid():
            password = serializer.validated_data.get('password')
            customer = serializer.save()
            customer.set_password(password)
            customer.save()
        else:
            raise serializers.ValidationError(serializer.errors)


    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            self.create_customer(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CustomerLogin(APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request):
        data = request.data
        serializer = CustomerLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.check_user(data)
            login(request, user)
            return Response(serializer.data, status=status.HTTP_200_OK)


class LogoutView(APIView):
    def post(self, request):
        logout(request)
        return Response({"message": "Logged out successfully"}, status=status.HTTP_200_OK)
