from django.db import models
from django.urls import reverse


class Books(models.Model):
    name = models.CharField(max_length=200, unique=True, null=True)
    product_id = models.IntegerField(null=True)
    author = models.CharField(max_length=200, null=True)
    type =  models.CharField(max_length=200, null=True)
    price=models.PositiveSmallIntegerField(null=True)
    genre =  models.CharField(max_length=200)


class Movies(models.Model):
    name=models.CharField(max_length=200)
    year=models.IntegerField()
    product_id=models.PositiveSmallIntegerField(unique=True)
    price=models.PositiveSmallIntegerField()
    genre= models.CharField(max_length=20)

# class Inventory(models.Model):
#     movies= models.ForeignKey(
#         Movies,
#         related_name="movies",
#         on_delete=models.CASCADE
#     )

#     books=models.ForeignKey(
#         Books,
#         related_name="books",
#         on_delete=models.CASCADE
#     )
