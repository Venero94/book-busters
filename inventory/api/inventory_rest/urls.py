from django.urls import path
from .views import api_list_movies, api_movies, list_books

urlpatterns = [
    path("movies/", api_list_movies, name="api_list_movies"),
    path("movies/<int:id>/", api_movies, name="api_movies"),
    path("books/", list_books, name="list_books"),
    path("books/<int:pk>/", list_books, name="list_books"),
    # path("inventory/", api_list_inventory, name="api_list_inventory")
]
