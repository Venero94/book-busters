from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# from .models import Genre, Books
from common.json import ModelEncoder
from .models import Movies, Books



class BooksEncoder(ModelEncoder):
    model = Books
    properties = ["name",
                "product_id",
                "author",
                "type",
                "price",
                "genre",
                "id"]



class MovieEncoder(ModelEncoder):
    model=Movies
    properties=[
        "name",
        "year",
        "product_id",
        "price",
        "genre",
        "id"
    ]


# class InventoryEncoder(ModelEncoder):
#     model= Inventory
#     properties=[
#         "movies",
#         "books",
#         "id"
#     ]
#     encoders={
#         "movies": MovieEncoder(),
#         "books": BooksEncoder(),
#     }


@require_http_methods(["GET","POST"])
def api_list_movies(request):
    if request.method=="GET":
        movies=Movies.objects.all()
        return JsonResponse(
            {"movies":movies},
            encoder=MovieEncoder
        )
    else:
        try:
            content=json.loads(request.body)
            movie=Movies.objects.create(**content)
            return JsonResponse(
                movie,
                encoder=MovieEncoder,
                safe=False
            )
        except:
            response=JsonResponse(
                {"message":"Invalid movie info man. Do it again bruh"}
            )
            response.status_code=400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def api_movies(request,id):
    if request.method=="GET":
        try:
            movie= Movies.objects.get(id=id)
            return JsonResponse(
                movie,
                encoder=MovieEncoder,
                safe=False
            )
        except Movies.DoesNotExist:
            return JsonResponse(
                {"message":"WHAT MOVIE IS THAT!!"},
                status=400,
            )
    elif request.method=="DELETE":
        try:
            count,_=Movies.objects.filter(id=id).delete()
            return JsonResponse({"deleted":"Movie is gone now"})
        except Movies.DoesNotExist:
            response=JsonResponse({"message":"What movie you talking about Willis?"})
            response.status_code=400
            return response
    else:
        try:
            content=json.loads(request.body)
            movies=Movies.objects.get(id=id)
        except Movies.DoesNotExist:
            return JsonResponse(
                {"message":"What movie you talking about Willis?"},
                status=400
            )
        Movies.objects.filter(id=id).update(**content)
        return JsonResponse(
            movies,
            encoder=MovieEncoder,
            safe=False
        )


# @require_http_methods(["GET", "POST"])
# def api_list_inventory(request):
#     if request.method=="GET":
#         inventory=Inventory.objects.all()
#         return JsonResponse(
#             {"inventory":inventory},
#             encoder=InventoryEncoder,
#             safe=False
#         )
#     else:
#         content=json.loads(request.body)
#         try:
#             books=Books.objects.get(id=content["books"])
#             content["books"]=books

#             movies= Movies.objects.get(id=content["movies"])
#             content["movies"]=movies
#             inventorys=Inventory.objects.create(**content)
#             return JsonResponse(
#                 inventorys,
#                 encoder=InventoryEncoder,
#                 safe=False
#             )
#         except:
#             response= JsonResponse(
#                 {"message":"Invalid. Please try again"}
#             )
#             response.status_code=400
#             return response
        #     BooksEncoder()
        # }


@require_http_methods(["GET", "POST", "DELETE"])
def list_books(request,pk=None):
    if request.method == "GET":
        book = Books.objects.all()
        return JsonResponse(
            {"book": book},
            encoder=BooksEncoder,
            safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        book = Books.objects.create(**content)
        return JsonResponse({"book": book},
                            encoder=BooksEncoder,
                            safe=False)
    if request.method == "DELETE":
        try:
            count, _ = Books.objects.filter(id=pk).delete()
            if count == 0:
                response = JsonResponse({"message": "Book not found"})
                response.status_code = 404
                return response
            return JsonResponse ({"deleted": count > 0})
        except:
            response = JsonResponse({"message": "Error deleting book"})
            response.status_code = 400
            return response
