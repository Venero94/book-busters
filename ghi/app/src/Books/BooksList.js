import React, { useState, useEffect }  from "react";

export default function BooksList() {
    const [book, setBook] = useState([]);

    async function LoadBook() {
        const response = await fetch("http://localhost:8100/api/books/");
        if (response.ok) {
            const data = await response.json();
            setBook(data.book)
        }
    }

    useEffect(() => {
        LoadBook();
    }, []);

    return (
        <div>
            <h1>Books!</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Book Title</th>
                        <th>Author</th>
                        <th>ISBN</th>
                        <th>Genre</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {book.map((bookdata) => {
                        return (
                            <tr key={bookdata.id}>
                                <td>{ bookdata.name }</td>
                                <td>{ bookdata.author }</td>
                                <td>{ bookdata.product_id }</td>
                                <td>{ bookdata.genre }</td>
                                <td>{ bookdata.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}