import React, { useState } from "react";

export default function BooksForm() {
  const [name, setName] = useState("");  
  const [product_id, setProduct_id] = useState("");
  const [author, setAuthor] = useState("");
  const [type, setType] = useState("");
  const [price, setPrice] = useState("");  
  const [genre, setGenre] = useState("");
  
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };
  
  const handleProduct_IdChange = (event) => {
    const value = event.target.value;
    setProduct_id(value);
  };
  
  const handleAuthorChange = (event) => {
    const value = event.target.value;
    setAuthor(value);
  };
  
  const handleTypeChange = (event) => {
    const value = event.target.value;
    setType(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const handleGenreChange = (event) => {
    const value = event.target.value;
    setGenre(value);
  };

  const data = {
    name: name,
    product_id : product_id,
    author: author,
    type: type,
    price: price,
    genre: genre
  };

  const booksURL = "http://localhost:8100/api/books/";
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(booksURL, fetchConfig);
    
    if (response.ok) {
      const newBook = await response.json();
      setName("");
      setProduct_id("");
      setAuthor("");
      setType("");
      setPrice("");
      setGenre("");
    }
  };

  return (
    <div>
      <div className="my-5 container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Grab a Book!</h1>
              <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                  <input
                    value={name} onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control"
                  />
                  <label htmlFor="first_name">Book Title</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={author} onChange={handleAuthorChange} placeholder="author" required type="text" name="author" id="author" className="form-control"
                  />
                  <label htmlFor="last_name">Author</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={product_id} onChange={handleProduct_IdChange} placeholder="product_id" required type="text" name="product_id" id="product_id" className="form-control"
                  />
                  <label htmlFor="last_name">ISBN</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={type} onChange={handleTypeChange} placeholder="type" required type="text" name="type" id="type" className="form-control"
                  />
                  <label htmlFor="last_name">Type</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={genre} onChange={handleGenreChange} placeholder="genre" required type="text" name="genre" id="genre" className="form-control"
                  />
                  <label htmlFor="last_name">Genre</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                        value={price} onChange={handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control"
                    />
                    <label htmlFor="model_name">Price</label>
                    </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}