import React, { useState } from 'react';

function CreateProduct() {
  const [productType, setProductType] = useState('book');
  const [formData, setFormData] = useState({
    name: '',
    product_id: '',
    author: '',
    genre: '',
    price: '',
    year: '',
  });

  const handleChange = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = productType === 'book' ? 'http://localhost:8100/api/books/' : 'http://localhost:8100/api/movies/';
    const data = productType === 'book' ? { ...formData, type: formData.genre } : formData;

    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });

      if (response.ok) {
        console.log('Product created successfully');
        setFormData({
          name: '',
          product_id: '',
          author: '',
          genre: '',
          price: '',
          year: '',
        });
      } else {
        console.error('Error creating product');
      }
    } catch (error) {
      console.error('Error creating product:', error);
    }
  };

  return (
    <div className="container">
      <h1>Create New Product</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="productType" className="form-label">Product Type</label>
          <select
            id="productType"
            name="productType"
            value={productType}
            onChange={(event) => setProductType(event.target.value)}
            className="form-control"
          >
            <option value="book">Book</option>
            <option value="movie">Movie</option>
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="name" className="form-label">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={formData.name}
            onChange={handleChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="product_id" className="form-label">Product ID</label>
          <input
            type="number"
            className="form-control"
            id="product_id"
            name="product_id"
            value={formData.product_id}
            onChange={handleChange}
            required
          />
        </div>
        {productType === 'book' && (
          <>
            <div className="mb-3">
              <label htmlFor="author" className="form-label">Author</label>
              <input
                type="text"
                className="form-control"
                id="author"
                name="author"
                value={formData.author}
                onChange={handleChange}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="genre" className="form-label">Genre</label>
              <input
                type="text"
                className="form-control"
                id="genre"
                name="genre"
                value={formData.genre}
                onChange={handleChange}
                required
              />
            </div>
          </>
        )}
        {productType === 'movie' && (
          <div className="mb-3">
            <label htmlFor="year" className="form-label">Year</label>
            <input
              type="number"
              className="form-control"
              id="year"
              name="year"
              value={formData.year}
              onChange={handleChange}
              required
            />
          </div>
        )}
        <div className="mb-3">
          <label htmlFor="price" className="form-label">Price</label>
          <input
            type="number"
            className="form-control"
            id="price"
            name="price"
            value={formData.price}
            onChange={handleChange}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">Create Product</button>
      </form>
    </div>
  );
}

export default CreateProduct;
