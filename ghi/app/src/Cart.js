import React, { useState, useEffect } from 'react';
import styles from './Cart.module.css';

function Cart() {
  const [cartItems, setCartItems] = useState([]);

  const fetchCartItems = async () => {
    try {
      const booksResponse = await fetch('http://localhost:8100/api/books/');
      const booksData = await booksResponse.json();
      const moviesResponse = await fetch('http://localhost:8100/api/movies/');
      const moviesData = await moviesResponse.json();

      const items = [
        ...booksData.map(book => ({ ...book, type: 'book' })),
        ...moviesData.map(movie => ({ ...movie, type: 'movie' })),
      ];
      setCartItems(items);
    } catch (error) {
      console.error('Error fetching cart items:', error);
    }
  };

  useEffect(() => {
    fetchCartItems();
  }, []);

  return (
    <div className="container">
      <h1 className="my-4">Your Cart</h1>
      <div className="list-group">
        {cartItems.map(item => (
          <div key={item.id} className={`list-group-item ${styles.cartItem}`}>
            <div className={styles.cartItemTitle}>{item.title} ({item.type})</div>
            <div className={styles.cartItemDetails}>
              <span className={styles.cartItemPrice}>${item.price} x {item.quantity}</span>
              <span className={styles.cartItemRemove} onClick={() => console.log('Remove item', item.id)}>Remove</span>
            </div>
          </div>
        ))}
      </div>
      <button className="btn btn-primary mt-4" onClick={() => window.location.href = '/checkout'}>Proceed to Checkout</button>
    </div>
  );
}

export default Cart;
