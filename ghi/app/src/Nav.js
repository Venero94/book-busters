import { NavLink } from 'react-router-dom';
import './index.css';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark custom-navbar">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">BookBuster</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/cart">View My Cart</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-product">Create Product</NavLink>
            </li>
            <li className='nav-item ml-auto' style={{ marginLeft: 'auto' }}>
              <NavLink to="/customers/create">
                <button className='btn btn-primary' style={{backgroundColor: '#0097b2'}}>Sign Up</button>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
