import React, {useState} from "react";

function MovieForm(){
  const [title, setTitle]=useState('')
  const [year, setYear]=useState('')
  const [genre, setGenre]=useState('')
  const [product, setProduct]=useState('')
  const [price, setPrice]=useState('')

  const handleTitleChange=(event)=>{
    const value = event.target.value
    setTitle(value)
  }

  const handleYearChange=(event)=>{
    const value = event.target.value
    setYear(value)
  }

  const handleGenreChange=(event)=>{
    const value = event.target.value
    setGenre(value)
  }

  const handleIDChange=(event)=>{
    const value = event.target.value
    setProduct(value)
  }

  const handlePriceChange=(event)=>{
    const value = event.target.value
    setPrice(value)
  }

  const handleSubmit=async(event)=>{
    event.preventDefault()
    const data={}
    data.name=title
    data.year=year
    data.genre=genre
    data.product_id=product
    data.price=price

    const url='http://localhost:8100/api/movies/'
    console.log({"data":data})
    const fetchConfig={
      method:"post",
      body:JSON.stringify(data),
      headers:{
        "Content-type":"application/json"
      }
    }

    const response = await fetch(url,fetchConfig)
    if (response.ok){
      const newMovie=await response.json()

      setTitle('')
      setYear('')
      setGenre('')
      setProduct('')
      setPrice('')
    }
  }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>MOVIE TIME!!</h1>
            <form onSubmit={handleSubmit} id="create-movie-form">
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="movie" required type="text" name="movie" id="movie" className="form-control"/>
                <label htmlFor="movie">Title</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleYearChange} value={year} placeholder="year" required type="number" name="year" id="year" className="form-control"/>
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleGenreChange} value={genre} placeholder="genre" required type="text" name="genre" id="genre" className="form-control"/>
                <label htmlFor="genre">Genre</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleIDChange} value={product} placeholder="product" required type="text" name="product" id="product" className="form-control"/>
                <label htmlFor="product">Product ID</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="price" required type="text" name="price" id="price" className="form-control"/>
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      )
}

export default MovieForm
