import React, {useState, useEffect} from "react";
import { NavLink } from "react-router-dom";

function MoviesList() {
  const [movies, setMovies] = useState([]);

  const fetchData = async () => {
    const response = await fetch('http://localhost:8100/api/movies/');

    if (response.ok) {
      const data = await response.json();
      setMovies(data.movies);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1>Movies List</h1>
      <div className="d-flex justify-content-end">
        <NavLink className="nav-link" to="/movies/new">
          <button type="button" className="btn btn-success" style={{backgroundColor: "#0097b2"}}>What we watching?</button>
        </NavLink>
      </div>
      <div className="row row-cols-1 row-cols-md-3 g-4">
        {movies?.map((movie) => {
          return (
            <div key={movie.id} className="col">
              <div className="card h-100">
                <div className="card-body">
                  <h5 className="card-title">{movie.name}</h5>
                  <p className="card-text">Release Date: {movie.year}</p>
                  <p className="card-text">Genre: {movie.genre}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}

export default MoviesList;





// import React, {useState, useEffect} from "react"
// import {NavLink} from "react-router-dom"

// function MoviesList(){
//     const [movies, setMovies]=useState([])

//     const fetchData=async()=>{
//         const response = await fetch('http://localhost:8100/api/movies/')

//         if (response.ok){
//             const data= await response.json()
//             setMovies(data.movies)
//         }
//     }

//     useEffect(()=>{
//         fetchData()
//     }, [])

//     return (
//         <>
//         <h1>Movies List</h1>
//         <div className="d-flex justify-content-end">
//           <NavLink className="nav-link" to="/movies/new">
//             <button type="button" className="btn btn-success">What we watching?</button>
//           </NavLink>
//         </div>
//           <table className="table table-striped">
//               <thead>
//                 <tr>
//                   <th>Title</th>
//                   <th>Release Date</th>
//                   <th>Genre</th>
//                 </tr>
//               </thead>
//               <tbody>
//                 {movies?.map(movie => {
//                   return (
//                     <tr key={movie.id}>
//                       <td>{movie.name}</td>
//                       <td>{movie.year}</td>
//                       <td>{movie.genre}</td>
//                     </tr>
//                   );
//                 })}
//               </tbody>
//             </table>
//         </>
//       )
// }

// export default MoviesList
