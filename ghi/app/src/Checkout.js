import React from 'react';
import styles from './Checkout.module.css';

function Checkout() {
  const handleSubmit = async (event) => {
    event.preventDefault();

    const orderData = {
      name: event.target.name.value,
      email: event.target.email.value,
      address: event.target.address.value,
    };

    try {
      const response = await fetch('http://localhost:8100/api/orders/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(orderData),
      });

      if (response.ok) {
        console.log('Order submitted successfully');
      } else {
        console.error('Error submitting order');
      }
    } catch (error) {
      console.error('Error submitting order:', error);
    }
  };

  return (
    <div className="container">
      <div className={`my-4 ${styles.formContainer}`}>
        <h1 className={styles.formTitle}>Checkout</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="name" className="form-label">Full Name</label>
            <input type="text" className="form-control" id="name" required />
          </div>
          <div className="mb-3">
            <label htmlFor="email" className="form-label">Email Address</label>
            <input type="email" className="form-control" id="email" required />
          </div>
          <div className="mb-3">
            <label htmlFor="address" className="form-label">Shipping Address</label>
            <input type="text" className="form-control" id="address" required />
          </div>
          <div className="mb-3">
            <label htmlFor="cardNumber" className="form-label">Credit Card Number</label>
            <input type="text" className="form-control" id="cardNumber" required />
          </div>
          <div className="mb-3">
            <label htmlFor="cardExpiry" className="form-label">Card Expiration Date</label>
            <input type="text" className="form-control" id="cardExpiry" required />
          </div>
          <div className="mb-3">
            <label htmlFor="cardCVV" className="form-label">Card CVV</label>
            <input type="text" className="form-control" id="cardCVV" required />
          </div>
          <button type="submit" className="btn btn-primary">Submit Order</button>
        </form>
      </div>
    </div>
  );
}

export default Checkout;
