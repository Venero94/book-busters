import React, { useState } from 'react';

export default function CustomerRegisterForm(){
    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}

        data.full_name = fullName
        data.email = email
        data.password = password
        data.phone_number = phoneNumber

        const customerCreationUrl = 'http://localhost:8080/api/customers/create/'
        const fetchCustomerConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(customerCreationUrl, fetchCustomerConfig)
        if (response.ok) {
            setFullName('');
            setEmail('');
            setPassword('');
            setPhoneNumber('');
        }
    }
    const handleNameChange = (event) => {
        const value = event.target.value
        setFullName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value);
    }
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }
    const handlePasswordChange = (event) => {
        const value = event.target.value
        setPassword(value)
    }

    return (
        <>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <div className='shadow p-4 mt-4'>
                        <h1>Customer Sign Up</h1>
                    <form onSubmit={handleSubmit} id='create-customer'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleNameChange} value={fullName} placeholder="Full Name" required type="text" name="fullname" id="full-name" className="form-control" />
                            <label htmlFor='fullname'>Full Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleEmailChange} value={email} placeholder="Email" required type="text" name="email" id="email" className="form-control" />
                            <label htmlFor='Email'>Email</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handlePasswordChange} value={password} placeholder="Password" required type="password" name="password" id="password" className="form-control" />
                            <label htmlFor='fullname'>Password</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handlePhoneNumberChange} value={phoneNumber} placeholder="Phone Number" required type="text" name="phonenumber" id="phonenumber" className="form-control" />
                            <label htmlFor='fullname'>Phone number</label>
                        </div>
                        <button className='btn btn-primary' style={{backgroundColor: '#0097b2'}}>Sign Up</button>
                    </form>
                    </div>
                </div>
            </div>
        </>
    )
}
