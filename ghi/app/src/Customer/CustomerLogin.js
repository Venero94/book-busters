import { Link } from "react-router-dom";
import React, { useState } from "react";
import logo from "../assets/images/logo.png";

export default function CustomerLoginForm() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.email = email;
    data.password = password;

    const customerLoginUrl = "http://localhost:8080/api/customers/login/";
    const fetchCustomerConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(customerLoginUrl, fetchCustomerConfig);
    if (response.ok) {
      setEmail("");
      setPassword("");
    }
  };
  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handlePasswordChange = (event) => {
    const value = event.target.value;
    setPassword(value);
  };

  return (
    <>
      <section className="vh-100 d-flex justify-content-center align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-9 col-lg-6 col-xl-5">
              <img src={logo} className="img-fluid mb-4" alt="logo" />
              <form onSubmit={handleSubmit}>
                {/* Email input */}
                <div className="form-outline mb-4">
                  <input
                    type="email"
                    id="form3Example3"
                    className="form-control form-control-lg"
                    placeholder="Email address"
                    onChange={handleEmailChange}
                  />
                  <label className="form-label" htmlFor="form3Example3">
                  </label>
                </div>

                {/* Password input */}
                <div className="form-outline mb-3">
                  <input
                    type="password"
                    id="form3Example4"
                    className="form-control form-control-lg"
                    placeholder="Password"
                    onChange={handlePasswordChange}
                  />
                  <label className="form-label" htmlFor="form3Example4">
                  </label>
                </div>
                <div className="text-center text-lg-start mt-4 pt-2">
                  <button
                    type="submit"
                    className="btn btn-primary btn-lg"
                    style={{ paddingLeft: "2.5rem", paddingRight: "2.5rem", backgroundColor: '#0097b2' }}
                  >
                    Login
                  </button>
                  <p className="small fw-bold mt-2 pt-1 mb-0">
                    Don't have an account?{" "}
                    <Link className="link-danger" to="/customers/create">
                      Register
                    </Link>
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
