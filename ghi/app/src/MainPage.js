import React from 'react';
import logo from './assets/images/logo.png';
import { Link } from 'react-router-dom';

function MainPage() {
  return (
      <div className="text-center">
        <img src={logo} alt="BookBuster" className="img-fluid" />
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The premiere solution for book and movie hunting.
          </p>
        </div>
        <Link to="/customers/login">
        <button className="btn btn-primary my-4" style={{ backgroundColor: '#0097b2' }}>Log in</button>
        </Link>
      </div>
  );
}

export default MainPage;
