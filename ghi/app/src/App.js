import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Cart from './Cart';
import Checkout from './Checkout';
import CreateProduct from './CreateProduct';

import CustomerRegisterForm from './Customer/CustomerRegister.js';
import MoviesList from './Movies/MovieList';
import MovieForm from './Movies/MovieForm';
import CustomerLoginForm from './Customer/CustomerLogin';
import BooksList from './Books/BooksList';
import BooksForm from './Books/BooksForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container-fluid">
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/checkout" element={<Checkout />} />
        <Route path="/create-product" element={<CreateProduct />} />
        <Route path="customers">
          <Route path="create" element={<CustomerRegisterForm />} />
          <Route path="login" element={<CustomerLoginForm />} />
        </Route>
        <Route path="movies">
          <Route path="" element={<MoviesList/>}/>
          <Route path="new" element={<MovieForm/>}/>
        </Route>
        <Route path="books">
            <Route index element={<BooksList />}></Route>
            <Route path="create" element={<BooksForm />}></Route>
          </Route>
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
