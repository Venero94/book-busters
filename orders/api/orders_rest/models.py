from django.db import models


class Order(models.Model):
    STATUS_CHOICES = (
        ('created', 'Created'),
        ('completed', 'Completed'),
        ('cancelled', 'Cancelled'),
    )

    customer_id = models.IntegerField()
    payment_id = models.IntegerField()
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='created')


class Payment(models.Model):
    STATUS_CHOICES = (
        ('pending', 'Pending'),
        ('completed', 'Completed'),
        ('declined', 'Declined')
    )

    order = models.OneToOneField(Order, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='pending')
